package com.lic.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;

import com.lic.dto.UserDto;

public class UserValidation {

	public static String validateUser(UserDto userDto) {
		CommonValidationDto commonValidationDto = new CommonValidationDto();
		Gson g = new Gson();
		String result = "";
		if (StringUtils.isEmpty(userDto.getEmail())) {
			commonValidationDto.setErrorCode("201");
			commonValidationDto.setErrorMessage("Email Field is required");
			commonValidationDto.setFieldName("email");
			commonValidationDto.setFieldValue(userDto.getEmail());
			commonValidationDto.setExpectedValue("savita@gmail.com");
			result = g.toJson(commonValidationDto);
			return result;
		} else if (!validate(userDto.getEmail())) {

			commonValidationDto.setErrorCode("201");
			commonValidationDto.setErrorMessage("Email Field is not valid.");
			commonValidationDto.setFieldName("email");
			commonValidationDto.setFieldValue(userDto.getEmail());
			commonValidationDto.setExpectedValue("savita@gmail.com");
			result = g.toJson(commonValidationDto);
			return result;
		} else if (StringUtils.isEmpty(userDto.getPassword())) {

			commonValidationDto.setErrorCode("201");
			commonValidationDto.setErrorMessage("Pasword Field is not valid.");
			commonValidationDto.setFieldName("password");
			commonValidationDto.setFieldValue(userDto.getEmail());
			result = g.toJson(commonValidationDto);

			return result;
		} else {

			return null;
		}

	}

	private  static boolean  validate(String emailStr) {

		Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}
	
	public static void main(String[] args) {
		
		System.out.println(validate("savitagmail.com"));
		
		
		
		
	}
}
