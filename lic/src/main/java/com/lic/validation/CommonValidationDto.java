package com.lic.validation;

public class CommonValidationDto {
	
	private String fieldName;
	private String fieldValue;
	private String expectedValue;
	private String minLength;
	private String maxLength;
	private String errorCode;
	private String errorMessage;
	
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getExpectedValue() {
		return expectedValue;
	}
	public void setExpectedValue(String expectedValue) {
		this.expectedValue = expectedValue;
	}
	public String getMinLength() {
		return minLength;
	}
	public void setMinLength(String minLength) {
		this.minLength = minLength;
	}
	public String getMaxLength() {
		return maxLength;
	}
	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	@Override
	public String toString() {
		return "CommonValidationDto [fieldName=" + fieldName + ", fieldValue=" + fieldValue + ", expectedValue="
				+ expectedValue + ", minLength=" + minLength + ", maxLength=" + maxLength + ", errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + "]";
	}
	
	
	

}
