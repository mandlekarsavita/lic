package com.lic.serviceimpl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lic.dto.ErroDetails;
import com.lic.dto.PaymentTransacionResponse;
import com.lic.dto.PaymentTransactionDto;
import com.lic.dto.PlanAppResponse;
import com.lic.dto.PlanDto;
import com.lic.dto.PurchasePlanDto;
import com.lic.dto.PurchasePlanResponse;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.PaymentTransaction;
import com.lic.entity.Plan;
import com.lic.entity.PurchasePlan;
import com.lic.entity.User;
import com.lic.repo.PaymentTransactionRepo;
import com.lic.repo.PlanRepo;
import com.lic.repo.PurchasePlanRepo;
import com.lic.repo.UserRepo;
import com.lic.service.PaymentTransactionService;
import com.lic.service.PlanAppService;
import com.lic.service.PurchasePlanService;
import com.lic.util.AppConstant;



@Service
public class PaymentTransactionServiceImpl implements PaymentTransactionService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	PlanRepo planRepo;

	@Autowired
	PaymentTransactionRepo paymentTransactionRepo;
	
	@Autowired
	PurchasePlanService purchasePlanService;
	
	
	@Transactional
	public PaymentTransacionResponse payment(PaymentTransactionDto paymentTransactionDto) {
		// TODO Auto-generated method stub
	
		ErroDetails er = new ErroDetails();
		PaymentTransacionResponse ap = new PaymentTransacionResponse();
		Long userId = paymentTransactionDto.getUserId();
		Long planId = paymentTransactionDto.getPlanId();
		long txnNo = (long) (Math.random() * 100000 + 3333300000L);
		List<User> userlist = new ArrayList<User>();
		Random random = new Random();
		// generate a random integer from 0 to 899, then add 100
		long trackid =random.nextInt(900) + 100;
		
		System.out.println("trackid>>>"+trackid);
		User user = null;
		Plan plan = null;
		try {
				Optional<User> userObj = userRepo.findById(userId);
				if (userObj.isPresent()) {
					user = userObj.get();
				}
				Optional<Plan> planObj = planRepo.findById(planId);
				if (userObj.isPresent()) {
					plan = planObj.get();
				}
				
				userlist = userRepo.getemailCheck(paymentTransactionDto.getEmail());
				if(!userlist.isEmpty()) {
					PaymentTransaction paymentTransaction = new PaymentTransaction();
					paymentTransaction.setUser(user);
					paymentTransaction.setPlan(plan);
					paymentTransaction.setTimeStamp(new Date());
					paymentTransaction.setAmount(paymentTransactionDto.getAmount());
					paymentTransaction.setEmail(paymentTransactionDto.getEmail());
					paymentTransaction.setTrackid(trackid);
					paymentTransaction.setTxnid(txnNo);
					paymentTransaction.setCustomerIP(paymentTransactionDto.getCustomerIP());
					paymentTransactionRepo.save(paymentTransaction);
					er.setErrorCode(AppConstant.SuccessCode);
					er.setErrorMessage(AppConstant.SuccessMessage);
					ap.setErroDetails(er);
					ap.setPaymentTransactionEntity(paymentTransaction);
				}
				else {
					er.setErrorCode(AppConstant.SuccessCode);
					er.setErrorMessage(AppConstant.EmailIDNotPresent);
					ap.setErroDetails(er);
				}
				

		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();
		}

		return ap;
	}


	
	
}
