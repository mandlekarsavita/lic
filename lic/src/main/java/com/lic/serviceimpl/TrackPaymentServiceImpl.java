package com.lic.serviceimpl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lic.dto.ErroDetails;
import com.lic.dto.PaymentTransacionResponse;
import com.lic.dto.PaymentTransactionDto;
import com.lic.dto.PlanAppResponse;
import com.lic.dto.PlanDto;
import com.lic.dto.PurchasePlanDto;
import com.lic.dto.PurchasePlanResponse;
import com.lic.dto.TrackPaymentResponse;
import com.lic.dto.TrackTransactionDto;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.PaymentTransaction;
import com.lic.entity.Plan;
import com.lic.entity.PurchasePlan;
import com.lic.entity.TrackPayment;
import com.lic.entity.User;
import com.lic.repo.PaymentTransactionRepo;
import com.lic.repo.PlanRepo;
import com.lic.repo.PurchasePlanRepo;
import com.lic.repo.TrackPaymentRepo;
import com.lic.repo.UserRepo;
import com.lic.service.PaymentTransactionService;
import com.lic.service.PlanAppService;
import com.lic.service.PurchasePlanService;
import com.lic.service.TrackPaymentService;
import com.lic.util.AppConstant;



@Service
public class TrackPaymentServiceImpl implements TrackPaymentService {
	
	@Autowired
	PaymentTransactionRepo paymentTransactionRepo;

	@Autowired
	TrackPaymentRepo trackPaymentRepo;
	
	@Transactional
	public TrackPaymentResponse trackpayment(TrackTransactionDto trackTransactionDto) {
		// TODO Auto-generated method stub
	
		ErroDetails er = new ErroDetails();
		TrackPaymentResponse ap = new TrackPaymentResponse();
		List<User> userlist = new ArrayList<User>();
		Long trackid = trackTransactionDto.getTrackid();
		User user = null;
		PaymentTransaction paymentTransaction = null;
		
		try {	
				
			Optional<PaymentTransaction> paymentTransactionObj = paymentTransactionRepo.findBytrackid(trackid);
			if (paymentTransactionObj.isPresent()) {
				paymentTransaction = paymentTransactionObj.get();
			}
			
				TrackPayment trackPayment = new TrackPayment();
					
					trackPayment.setTimeStamp(new Date());
					trackPayment.setTrack(paymentTransaction);
					trackPaymentRepo.save(trackPayment);
					er.setErrorCode(AppConstant.SuccessCode);
					er.setErrorMessage(AppConstant.SuccessMessage);
					ap.setErroDetails(er);
					ap.setTrackPayment(trackPayment);
			
//					er.setErrorCode(AppConstant.SuccessCode);
//					er.setErrorMessage(AppConstant.EmailIDNotPresent);
//					ap.setErroDetails(er);
				
				

		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();
		}

		return ap;
	}


	
	
}
