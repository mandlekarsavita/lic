package com.lic.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lic.dto.ErroDetails;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.User;
import com.lic.repo.UserRepo;
import com.lic.service.LoginService;
import com.lic.util.AppConstant;
import com.lic.validation.UserValidation;
import com.google.gson.Gson;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	UserRepo userRepo;

	@Transactional
	public String login(UserDto userDto) {
		// TODO Auto-generated method stub
		List<User> userlist = new ArrayList<User>();
		ErroDetails er = new ErroDetails();
		String out = "";
		UserAppResponse ap = new UserAppResponse();
		Gson g = new Gson();

		out = UserValidation.validateUser(userDto);

		// iF NOT eMPLTY MEANS ANY OF THE VALIDATION IS aPPLIED ( Input is invalid)
		if (StringUtils.isNotEmpty((out))) {

			return out;
		}
		try {
			userlist = userRepo.getlogin(userDto.getEmail(), userDto.getPassword());
			if (!userlist.isEmpty()) {
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.SuccessMessage);
				ap.setListentity(userlist);
				ap.setErroDetails(er);

			} else {
				er.setErrorCode(AppConstant.ErrorCode);
				er.setErrorMessage(AppConstant.FailedMessage);
				ap.setErroDetails(er);
			}

		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();
		}

		out = g.toJson(ap);
		return out;
	}

	private User toaddUserEntity(UserDto userDto) {
		// TODO Auto-generated method stub
		User userentity = new User();
		userentity.setId(userDto.getId());
		userentity.setFirstName(userDto.getFirstName());
		userentity.setLastName(userDto.getLastName());
		userentity.setAdharNo(userDto.getAdharNo());
		userentity.setAge(userDto.getAge());
		userentity.setEmail(userDto.getEmail());
		userentity.setCreatedAt(new Date());
		userentity.setUpdateAt(new Date());
		return userentity;

	}

}
