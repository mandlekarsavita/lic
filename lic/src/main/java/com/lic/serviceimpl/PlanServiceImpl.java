package com.lic.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lic.dto.ErroDetails;
import com.lic.dto.PlanAppResponse;
import com.lic.dto.PlanDto;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.Plan;
import com.lic.entity.User;
import com.lic.repo.PlanRepo;
import com.lic.service.PlanAppService;
import com.lic.util.AppConstant;

@Service
public class PlanServiceImpl implements PlanAppService {

	@Autowired
	PlanRepo planRepo;

	@Transactional
	public PlanAppResponse addPlan(PlanDto planDto) {
		// TODO Auto-generated method stub
		List<Plan> planlist = new ArrayList<Plan>();
		ErroDetails er = new ErroDetails();
		PlanAppResponse ap = new PlanAppResponse();
		try {
			Plan planenity = toaddPlanEntity(planDto);
			Plan plancheck = planRepo.findByName(planDto.getName().trim());
			if (Objects.nonNull(plancheck)) {
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.ErrorMessage);
				ap.setErroDetails(er);
				ap.setPlanentity(planlist);

			} else {
				Plan ppDB = planRepo.save(planenity);
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.SuccessMessage);
				ap.setErroDetails(er);
				planlist.add(ppDB);
				ap.setPlanentity(planlist);
			}

		} catch (Exception e) {

			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();
		}

		return ap;
	}

	private Plan toaddPlanEntity(PlanDto planDto) {
		// TODO Auto-generated method stub
		Plan planentity = new Plan();
		planentity.setId(planDto.getId());
		planentity.setName(planDto.getName());
		planentity.setMaturityPeriod(planDto.getMaturityPeriod());
		planentity.setTimeStamp(new Date());
		return planentity;

	}

	@Transactional
	public PlanAppResponse deletePlan(Long id) {
		// TODO Auto-generated method stub
		ErroDetails er = new ErroDetails();
		PlanAppResponse ap = new PlanAppResponse();
		Plan plan = null;
		try {
			Optional<Plan> planObj = planRepo.findById(id);

			if (planObj.isPresent()) {
				plan = planObj.get();

				if (Objects.nonNull(plan)) {
					planRepo.delete(plan);
					er.setErrorCode(AppConstant.SuccessCode);
					er.setErrorMessage(AppConstant.SuccessMessage);
					ap.setErroDetails(er);

				}
			} else {

				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage("Plan Doen't exists");
				ap.setErroDetails(er);
			}
		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();

		}
		return ap;

	}

	@Override
	public PlanAppResponse updatePlan(PlanDto planDto) {
		// TODO Auto-generated method stub
		Plan plan = null;
		ErroDetails er = new ErroDetails();
		PlanAppResponse ap = new PlanAppResponse();
		try {

			Plan planEntity = toaddPlanEntity(planDto);
			Optional<Plan> opn = planRepo.findById(planEntity.getId());

			try {
				if (opn != null) {
					plan = (Plan) opn.get();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (plan != null) {
				planRepo.save(planEntity);
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.SuccessMessage);
			} else {
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.FailedToUpdateMessage);
			}
			ap.setErroDetails(er);
		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();

		}
		return ap;
	}

}
