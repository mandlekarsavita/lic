package com.lic.serviceimpl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lic.dto.ErroDetails;
import com.lic.dto.PlanAppResponse;
import com.lic.dto.PlanDto;
import com.lic.dto.PurchasePlanDto;
import com.lic.dto.PurchasePlanResponse;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.Plan;
import com.lic.entity.PurchasePlan;
import com.lic.entity.User;
import com.lic.repo.PlanRepo;
import com.lic.repo.PurchasePlanRepo;
import com.lic.repo.UserRepo;
import com.lic.service.PlanAppService;
import com.lic.service.PurchasePlanService;
import com.lic.util.AppConstant;



@Service
public class PurchaseServiceImpl implements PurchasePlanService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	PlanRepo planRepo;

	@Autowired
	PurchasePlanRepo purchasePlanRepo;
	
	@Autowired
	PurchasePlanService purchasePlanService;
	
	
	@Transactional
	public PurchasePlanResponse addpurchaseplan(PurchasePlanDto purchasePlanDto) {
		// TODO Auto-generated method stub
	
		ErroDetails er = new ErroDetails();
		PurchasePlanResponse ap = new PurchasePlanResponse();
		Long userId = purchasePlanDto.getUserId();
		Long planId = purchasePlanDto.getPlanId();
		
		User user = null;
		Plan plan = null;
		try {
				Optional<User> userObj = userRepo.findById(userId);
				if (userObj.isPresent()) {
					user = userObj.get();
				}
				Optional<Plan> planObj = planRepo.findById(planId);
				if (userObj.isPresent()) {
					plan = planObj.get();
				}
				
				PurchasePlan p1 = new PurchasePlan();
				p1.setUser(user);
				p1.setPlan(plan);
				p1.setTimeStamp(new Date());
				p1.setPlanName(plan.getName());
				p1.setUserName(user.getFirstName()+"\t"+user.getLastName());
				purchasePlanRepo.save(p1);
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.SuccessMessage);
				ap.setErroDetails(er);

		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();
		}

		return ap;
	}

	
}
