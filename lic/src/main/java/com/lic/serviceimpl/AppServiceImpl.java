package com.lic.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lic.dto.ErroDetails;
import com.lic.dto.PurchasePlanDto;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.PurchasePlan;
import com.lic.entity.User;
import com.lic.repo.UserRepo;
import com.lic.service.AppService;
import com.lic.util.AppConstant;

@Service
public class AppServiceImpl implements AppService {

	@Autowired
	UserRepo userRepo;

	@Transactional
	public UserAppResponse addUser(UserDto userDto) {
		// TODO Auto-generated method stub
		List<User> userlist = new ArrayList<User>();
		ErroDetails er = new ErroDetails();
		UserAppResponse ap = new UserAppResponse();
		try {
			User userenity = toaddUserEntity(userDto);
			User usercheck = userRepo.findByEmail(userDto.getEmail().trim());
			if (Objects.nonNull(usercheck)) {
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.ErrorMessage);
				ap.setErroDetails(er);
				ap.setListentity(userlist);

			} else {
				User ppDB = userRepo.save(userenity);
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.SuccessMessage);
				ap.setErroDetails(er);
				userlist.add(ppDB);
				ap.setListentity(userlist);
			}

		} catch (Exception e) {

			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();
		}

		return ap;
	}

	private User toaddUserEntity(UserDto userDto) {
		// TODO Auto-generated method stub
		User userentity = new User();
		userentity.setId(userDto.getId());
		userentity.setFirstName(userDto.getFirstName());
		userentity.setLastName(userDto.getLastName());
		userentity.setAdharNo(userDto.getAdharNo());
		userentity.setAge(userDto.getAge());
		userentity.setEmail(userDto.getEmail());
		userentity.setPassword(userDto.getPassword());
		userentity.setCreatedAt(new Date());
		userentity.setUpdateAt(new Date());
		return userentity;

	}

	@Transactional
	public UserAppResponse deleteUser(Long id) {
		// TODO Auto-generated method stub
		ErroDetails er = new ErroDetails();
		UserAppResponse ap = new UserAppResponse();
		User user = null;
		try {

			Optional<User> userObj = userRepo.findById(id);

			if (userObj.isPresent()) {
				user = userObj.get();
			}

			if (Objects.nonNull(user)) {
				userRepo.delete(user);
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.SuccessMessage);
				ap.setErroDetails(er);

			} else {
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.DeleteErrorMessage);
				ap.setErroDetails(er);
			}
		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();

		}
		return ap;

	}

	@Transactional
	public UserAppResponse updateUser(UserDto userDto) {
		// TODO Auto-generated method stub

		User user = null;
		ErroDetails er = new ErroDetails();
		UserAppResponse ap = new UserAppResponse();
		try {

			user = userRepo.findByEmail(userDto.getEmail());

			if (Objects.nonNull(user)) {
				user.setEmail(userDto.getEmail());
				user.setFirstName(userDto.getFirstName());
				userRepo.save(user);
				er.setErrorCode(AppConstant.SuccessCode);
				er.setErrorMessage(AppConstant.SuccessMessage);
			} else {
				er.setErrorCode(AppConstant.FailedRespnseCode);
				er.setErrorMessage(AppConstant.FailedToUpdateMessage);
			}
			ap.setErroDetails(er);
		} catch (Exception e) {
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();

		}
		return ap;
	}

	@Override
	public UserAppResponse showuser() {
		// TODO Auto-generated method stub
		List<User> list = null;
		List<UserDto> list2 = null;
		ErroDetails er = null;
		UserAppResponse ap = new UserAppResponse();
		try {

			list = (List<User>) userRepo.findAll();
			er = new ErroDetails();
			er.setErrorCode(AppConstant.SuccessCode);
			er.setErrorMessage(AppConstant.SuccessMessage);
			ap.setErroDetails(er);
			list2 = toDto(list);
			ap.setListdto(list2);
		} catch (Exception e) {
			er = new ErroDetails();
			er.setErrorCode(AppConstant.ErrorCode);
			er.setErrorMessage(AppConstant.FailedMessage);
			ap.setErroDetails(er);
			e.printStackTrace();
		}
		return ap;
	}

	private List<UserDto> toDto(List<User> list) {
		List<UserDto> list2 = new ArrayList<>();

		for (User emp : list) {
			UserDto empDto = toDto(emp);
			list2.add(empDto);
		}
		return list2;
	}

	public UserDto toDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setAdharNo(user.getAdharNo());
		userDto.setAge(user.getAge());
		userDto.setEmail(user.getEmail());
		userDto.setCreatedAt(new Date());
		userDto.setUpdateAt(new Date());
		return userDto;
	}

}
