package com.lic.util;

public class AppConstant {
	
	public static String ErrorMessage="Failed!! Already present in database";
	public static String SuccessMessage="SUCCESS";
	public static String FailedMessage="Failed";
	public static String SuccessCode="200";
	public static String ErrorCode="100";
	public static String FailedRespnseCode="601";
	public static String DeleteErrorMessage="Failed!! Data Not present in database";
	public static String FailedToUpdateMessage="FAILED TO UPDATE";
	public static String EmailIDNotPresent="Email id is not Present";
	

}
