package com.lic.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "track_payment")
public class TrackPayment {

	public TrackPayment() {

	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
//	@Column(name = "track_id")
//	private Long  trackid;
	
	private Date timeStamp;
	
	@ManyToOne
	@JoinColumn(name = "track_id")
	private PaymentTransaction track;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public PaymentTransaction getTrack() {
		return track;
	}

	public void setTrack(PaymentTransaction track) {
		this.track = track;
	}

	@Override
	public String toString() {
		return "TrackPayment [id=" + id + ", timeStamp=" + timeStamp + ", track=" + track + "]";
	}
	
	
}

	