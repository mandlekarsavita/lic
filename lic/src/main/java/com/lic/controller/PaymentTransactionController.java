package com.lic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lic.dto.PaymentTransacionResponse;
import com.lic.dto.PaymentTransactionDto;
import com.lic.dto.PurchasePlanDto;
import com.lic.repo.PlanRepo;
import com.lic.repo.PurchasePlanRepo;
import com.lic.repo.UserRepo;
import com.lic.service.PaymentTransactionService;

@RestController
public class PaymentTransactionController {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	PlanRepo planRepo;

	@Autowired
	PurchasePlanRepo purchasePlanRepo;
	
	@Autowired
	PaymentTransactionService paymentTransactionService;
	
	@PostMapping("/payment") 
	public PaymentTransacionResponse payment(@RequestBody PaymentTransactionDto paymentTransactionDto) {
		return paymentTransactionService.payment(paymentTransactionDto);
	}

	
}
