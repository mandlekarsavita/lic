package com.lic.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lic.dto.PurchasePlanDto;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.User;
import com.lic.repo.UserRepo;
import com.lic.service.AppService;

@RestController
public class AppUserController {

	@Autowired
	UserRepo userRepo;
	
	@Autowired
	AppService appService;
	
	@PostMapping("/adduser") //
	public UserAppResponse addUser(@RequestBody UserDto userDto) {
		return appService.addUser(userDto);
	}
	
	@PostMapping("/deleteuser") //
	public UserAppResponse delete(@RequestBody UserDto userDto) {
		return appService.deleteUser(userDto.getId());
	}
	
	@PostMapping("/updateuser") //
	public UserAppResponse update(@RequestBody UserDto userDto) {        
		return appService.updateUser(userDto);
	}

	@GetMapping("/showuser") //
	public UserAppResponse show() {
		return appService.showuser();
	}
	@PostMapping("/adduser1")
	public String addUser(@RequestBody User user) {

		String out = "";

		try {
			user.setCreatedAt(new Date());
			user.setUpdateAt(new Date());
			userRepo.save(user);
			out = "success";

		} catch (Exception e) {
			e.printStackTrace();
			out = "fail";

		}
		return out;

	}
}
