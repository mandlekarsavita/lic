package com.lic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lic.dto.PlanAppResponse;
import com.lic.dto.PlanDto;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.Plan;
import com.lic.repo.PlanRepo;
import com.lic.service.PlanAppService;


@RestController
public class PlanController {

	@Autowired
	PlanRepo planRepo;
	
	@Autowired
	PlanAppService planAppService;
	
	
	@PostMapping("/addPlan") //
	public PlanAppResponse addPlan(@RequestBody PlanDto planDto) {
		return planAppService.addPlan(planDto);
	}
	
	@PostMapping("/deletePlan") //
	public PlanAppResponse deletePlan(@RequestBody PlanDto planDto) {
		return planAppService.deletePlan(planDto.getId());
	}
	
	@PostMapping("/updatePlan") //
	public PlanAppResponse updatePlan(@RequestBody PlanDto planDto) {        
		return planAppService.updatePlan(planDto);
	}

//	@PostMapping("/addplan1")
//	public String addUser(@RequestBody Plan plan) {
//
//		String out = "";
//
//		try {
//
//			planRepo.save(plan);
//			out = "success";
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			out = "fail";
//
//		}
//		return out;
//
//	}
}
