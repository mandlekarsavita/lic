package com.lic.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lic.dto.PlanAppResponse;
import com.lic.dto.PlanDto;
import com.lic.dto.PurchasePlanDto;
import com.lic.dto.PurchasePlanResponse;
import com.lic.entity.Plan;
import com.lic.entity.PurchasePlan;
import com.lic.entity.User;
import com.lic.repo.PlanRepo;
import com.lic.repo.PurchasePlanRepo;
import com.lic.repo.UserRepo;
import com.lic.service.PurchasePlanService;

@RestController
public class PurchasePlanController {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	PlanRepo planRepo;

	@Autowired
	PurchasePlanRepo purchasePlanRepo;
	
	@Autowired
	PurchasePlanService purchasePlanService;
	
	@PostMapping("/addpurchaseplan") //
	public PurchasePlanResponse addPlan(@RequestBody PurchasePlanDto purchasePlanDto) {
		return purchasePlanService.addpurchaseplan(purchasePlanDto);
	}

	@PostMapping("/purchase_plan1")
	public String purchasePlan(@RequestBody PurchasePlanDto purchasePlanDto) {

		Long userId = purchasePlanDto.getUserId();
		Long planId = purchasePlanDto.getPlanId();
		User user = null;
		Plan plan = null;
		try {

			Optional<User> userObj = userRepo.findById(userId);

			if (userObj.isPresent()) {
				user = userObj.get();
			}

			Optional<Plan> planObj = planRepo.findById(planId);

			if (userObj.isPresent()) {
				plan = planObj.get();
			}

			PurchasePlan p1 = new PurchasePlan();
			p1.setUser(user);
			p1.setPlan(plan);

			purchasePlanRepo.save(p1);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";

	}

}
