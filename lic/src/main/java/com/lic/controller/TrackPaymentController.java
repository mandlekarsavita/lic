package com.lic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lic.dto.PurchasePlanDto;
import com.lic.dto.PurchasePlanResponse;
import com.lic.dto.TrackPaymentResponse;
import com.lic.dto.TrackTransactionDto;
import com.lic.service.PurchasePlanService;
import com.lic.service.TrackPaymentService;

@RestController
public class TrackPaymentController {
	
	@Autowired
	TrackPaymentService trackPaymentService;

	
	@PostMapping("/trackpayment") //
	public TrackPaymentResponse trackpayment(@RequestBody TrackTransactionDto trackTransactionDto) {
		return trackPaymentService.trackpayment(trackTransactionDto);
		
	}

	
}
