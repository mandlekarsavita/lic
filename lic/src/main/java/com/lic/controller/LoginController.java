package com.lic.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lic.dto.PurchasePlanDto;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;
import com.lic.entity.User;
import com.lic.repo.UserRepo;
import com.lic.service.AppService;
import com.lic.service.LoginService;

@RestController
public class LoginController {

	@Autowired
	UserRepo userRepo;
	
	@Autowired
	LoginService loginService;
	
	@PostMapping("/login") //
	public String login(@RequestBody UserDto userDto) {
		return loginService.login(userDto);
	}
	
	
}
