package com.lic.repo;

import org.springframework.data.repository.CrudRepository;

import com.lic.entity.PurchasePlan;

public interface PurchasePlanRepo extends CrudRepository<PurchasePlan, Long> {

}
