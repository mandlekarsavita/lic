package com.lic.repo;

import org.springframework.data.repository.CrudRepository;

import com.lic.entity.Plan;
import com.lic.entity.User;

public interface PlanRepo  extends CrudRepository<Plan, Long> {
	
	Plan findByName(String name);
}
