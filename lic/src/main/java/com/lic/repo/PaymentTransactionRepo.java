package com.lic.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.lic.entity.PaymentTransaction;
import com.lic.entity.Plan;
import com.lic.entity.PurchasePlan;
import com.lic.entity.User;

public interface PaymentTransactionRepo extends CrudRepository<PaymentTransaction, Long> {
	
	//Optional<PaymentTransaction> findBytrackid(Long trackid);
	
	@Query(value = "select * from payment_txn payment where trackid=:trackid", nativeQuery = true)
	Optional<PaymentTransaction> findBytrackid(@Param("trackid") Long trackid);
}
