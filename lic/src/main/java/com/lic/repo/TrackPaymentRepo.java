package com.lic.repo;

import org.springframework.data.repository.CrudRepository;

import com.lic.entity.PaymentTransaction;
import com.lic.entity.PurchasePlan;
import com.lic.entity.TrackPayment;

public interface TrackPaymentRepo extends CrudRepository<TrackPayment, Long> {

}
