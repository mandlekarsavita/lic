package com.lic.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.lic.entity.User;



public interface UserRepo extends CrudRepository<User, Long> {
	
	User findByEmail(String email);
	
	//@Query(value = "select * from app_user appuser where email=:email", nativeQuery = true)

	@Query(value = "select * from app_user appuser where email=:email and password=:password", nativeQuery = true)
	List<User> getlogin(@Param("email") String email,
			            @Param("password") String password);
	
	
	@Query(value = "select * from app_user appuser where email=:email", nativeQuery = true)
	List<User> getemailCheck(@Param("email") String email);


}
