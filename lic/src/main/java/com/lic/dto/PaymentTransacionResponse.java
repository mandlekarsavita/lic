package com.lic.dto;

import java.util.List;

import com.lic.entity.PaymentTransaction;
import com.lic.entity.PurchasePlan;

public class PaymentTransacionResponse {

	private ErroDetails erroDetails;

	private List<PaymentTransactionDto> paymentTransactionDto;
	
	private PaymentTransaction paymentTransactionEntity;
	
	

	public PaymentTransaction getPaymentTransactionEntity() {
		return paymentTransactionEntity;
	}

	public void setPaymentTransactionEntity(PaymentTransaction paymentTransactionEntity) {
		this.paymentTransactionEntity = paymentTransactionEntity;
	}

	public ErroDetails getErroDetails() {
		return erroDetails;
	}

	public void setErroDetails(ErroDetails erroDetails) {
		this.erroDetails = erroDetails;
	}

	public List<PaymentTransactionDto> getPaymentTransactionDto() {
		return paymentTransactionDto;
	}

	public void setPaymentTransactionDto(List<PaymentTransactionDto> paymentTransactionDto) {
		this.paymentTransactionDto = paymentTransactionDto;
	}



	@Override
	public String toString() {
		return "PaymentTransacionResponse [erroDetails=" + erroDetails + ", paymentTransactionDto="
				+ paymentTransactionDto + ", paymentTransactionEntity=" + paymentTransactionEntity + "]";
	}

	
	

}
