package com.lic.dto;

import java.util.Date;

import javax.persistence.Column;

public class TrackTransactionDto {
	private Long  trackid;
	private Date timeStamp;
	
	

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Long getTrackid() {
		return trackid;
	}

	public void setTrackid(Long trackid) {
		this.trackid = trackid;
	}

	@Override
	public String toString() {
		return "TrackTransactionDto [trackid=" + trackid + ", timeStamp=" + timeStamp + "]";
	}

	
	
	
	
	
}
