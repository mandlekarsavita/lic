package com.lic.dto;

import java.util.List;

import com.lic.entity.User;

public class UserAppResponse {

	private ErroDetails erroDetails;

	private List<UserDto> listdto;
	
	private List<User> listentity;

	public ErroDetails getErroDetails() {
		return erroDetails;
	}

	public void setErroDetails(ErroDetails erroDetails) {
		this.erroDetails = erroDetails;
	}

	public List<UserDto> getListdto() {
		return listdto;
	}

	public void setListdto(List<UserDto> listdto) {
		this.listdto = listdto;
	}

	public List<User> getListentity() {
		return listentity;
	}

	public void setListentity(List<User> listentity) {
		this.listentity = listentity;
	}

	@Override
	public String toString() {
		return "UserAppResponse [erroDetails=" + erroDetails + ", listdto=" + listdto + ", listentity=" + listentity
				+ "]";
	}


}
