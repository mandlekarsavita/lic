package com.lic.dto;

import java.util.List;

import com.lic.entity.PaymentTransaction;
import com.lic.entity.PurchasePlan;
import com.lic.entity.TrackPayment;

public class TrackPaymentResponse {

	private ErroDetails erroDetails;

	private List<TrackTransactionDto> trackTransactionDto;
	
	private TrackPayment trackPayment;

	private PaymentTransaction paymentTransaction;
	
	
	
	public PaymentTransaction getPaymentTransaction() {
		return paymentTransaction;
	}

	public void setPaymentTransaction(PaymentTransaction paymentTransaction) {
		this.paymentTransaction = paymentTransaction;
	}

	public ErroDetails getErroDetails() {
		return erroDetails;
	}

	public void setErroDetails(ErroDetails erroDetails) {
		this.erroDetails = erroDetails;
	}

	public List<TrackTransactionDto> getTrackTransactionDto() {
		return trackTransactionDto;
	}

	public void setTrackTransactionDto(List<TrackTransactionDto> trackTransactionDto) {
		this.trackTransactionDto = trackTransactionDto;
	}

	public TrackPayment getTrackPayment() {
		return trackPayment;
	}

	public void setTrackPayment(TrackPayment trackPayment) {
		this.trackPayment = trackPayment;
	}

	@Override
	public String toString() {
		return "TrackPaymentResponse [erroDetails=" + erroDetails + ", trackTransactionDto=" + trackTransactionDto
				+ ", trackPayment=" + trackPayment + ", paymentTransaction=" + paymentTransaction + "]";
	}
	
	

	

	
	

}
