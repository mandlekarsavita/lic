package com.lic.dto;

import java.util.List;

import com.lic.entity.PurchasePlan;

public class PurchasePlanResponse {

	private ErroDetails erroDetails;

	private List<PurchasePlanDto> plandto;
	
	private List<PurchasePlan> planentity;

	public ErroDetails getErroDetails() {
		return erroDetails;
	}

	public void setErroDetails(ErroDetails erroDetails) {
		this.erroDetails = erroDetails;
	}

	public List<PurchasePlanDto> getPlandto() {
		return plandto;
	}

	public void setPlandto(List<PurchasePlanDto> plandto) {
		this.plandto = plandto;
	}

	public List<PurchasePlan> getPlanentity() {
		return planentity;
	}

	public void setPlanentity(List<PurchasePlan> planentity) {
		this.planentity = planentity;
	}

	@Override
	public String toString() {
		return "PlanAppResponse [erroDetails=" + erroDetails + ", plandto=" + plandto + ", planentity=" + planentity
				+ "]";
	}

	

}
