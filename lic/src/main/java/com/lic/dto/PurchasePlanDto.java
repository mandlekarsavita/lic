package com.lic.dto;

public class PurchasePlanDto {

	private Long userId;
	private Long planId;
	
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getPlanId() {
		return planId;
	}
	public void setPlanId(Long planId) {
		this.planId = planId;
	}
	@Override
	public String toString() {
		return "PurchasePlanDto [userId=" + userId + ", planId=" + planId + ", planName="  + "]";
	}
	
	
}
