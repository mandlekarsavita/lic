package com.lic.dto;

import javax.persistence.Column;

public class PaymentTransactionDto {

	private Long userId;
	private Long planId;
	private String amount;
	private String  txnid;
	private String  trackid;
	private String  email;
	private String  customerIP;
	
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public String getTrackid() {
		return trackid;
	}
	public void setTrackid(String trackid) {
		this.trackid = trackid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCustomerIP() {
		return customerIP;
	}
	public void setCustomerIP(String customerIP) {
		this.customerIP = customerIP;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getPlanId() {
		return planId;
	}
	public void setPlanId(Long planId) {
		this.planId = planId;
	}
	@Override
	public String toString() {
		return "PaymentTransactionDto [userId=" + userId + ", planId=" + planId + ", amount=" + amount + ", txnid="
				+ txnid + ", trackid=" + trackid + ", email=" + email + ", customerIP=" + customerIP + "]";
	}
	
	
}
