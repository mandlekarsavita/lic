package com.lic.dto;

import java.util.List;

import com.lic.entity.Plan;

public class PlanAppResponse {

	private ErroDetails erroDetails;

	private List<PlanDto> plandto;
	
	private List<Plan> planentity;

	public ErroDetails getErroDetails() {
		return erroDetails;
	}

	public void setErroDetails(ErroDetails erroDetails) {
		this.erroDetails = erroDetails;
	}

	public List<PlanDto> getPlandto() {
		return plandto;
	}

	public void setPlandto(List<PlanDto> plandto) {
		this.plandto = plandto;
	}

	public List<Plan> getPlanentity() {
		return planentity;
	}

	public void setPlanentity(List<Plan> planentity) {
		this.planentity = planentity;
	}

	@Override
	public String toString() {
		return "PlanAppResponse [erroDetails=" + erroDetails + ", plandto=" + plandto + ", planentity=" + planentity
				+ "]";
	}

	

}
