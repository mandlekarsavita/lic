package com.lic.dto;

public class ErroDetails {

	
	
	private String errorCode;
	
	private String errorMessage;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "ErroDetails [errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
	}
	
	
	
	
	
}
