package com.lic.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


public class PlanDto {
	
	private Long id;
	
	private String maturityPeriod;
	
	private String  name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMaturityPeriod() {
		return maturityPeriod;
	}

	public void setMaturityPeriod(String maturityPeriod) {
		this.maturityPeriod = maturityPeriod;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "PlanDto [id=" + id + ", maturityPeriod=" + maturityPeriod + ", name=" + name + "]";
	}
 
	


	

	
	
}
