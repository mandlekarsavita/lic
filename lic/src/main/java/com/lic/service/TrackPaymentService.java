package com.lic.service;

import com.lic.dto.PaymentTransacionResponse;
import com.lic.dto.PaymentTransactionDto;
import com.lic.dto.PurchasePlanDto;
import com.lic.dto.PurchasePlanResponse;
import com.lic.dto.TrackPaymentResponse;
import com.lic.dto.TrackTransactionDto;

public interface TrackPaymentService {
	
public TrackPaymentResponse trackpayment(TrackTransactionDto trackTransactionDto);



}
