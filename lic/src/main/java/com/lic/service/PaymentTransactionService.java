package com.lic.service;

import com.lic.dto.PaymentTransacionResponse;
import com.lic.dto.PaymentTransactionDto;
import com.lic.dto.PurchasePlanDto;
import com.lic.dto.PurchasePlanResponse;

public interface PaymentTransactionService {
	
public PaymentTransacionResponse payment(PaymentTransactionDto paymentTransactionDto);



}
