package com.lic.service;

import com.lic.dto.PlanAppResponse;
import com.lic.dto.PlanDto;
import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;

public interface PlanAppService {
	
public PlanAppResponse addPlan(PlanDto planDto);

public PlanAppResponse deletePlan(Long id);

public PlanAppResponse updatePlan(PlanDto planDto);

//public UserAppResponse showuser(UserDto userDto);


}
