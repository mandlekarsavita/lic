package com.lic.service;

import com.lic.dto.UserAppResponse;
import com.lic.dto.UserDto;

public interface AppService {
	
public UserAppResponse addUser(UserDto userDto);

public UserAppResponse deleteUser(Long id);

public UserAppResponse updateUser(UserDto userDto);

public UserAppResponse showuser();


}
